package io.nuls.api.provider;

import lombok.Data;

/**
 * @Author: zhoulijun
 * @Time: 2019-03-07 15:11
 * @Description: 功能描述
 */
@Data
public class BaseReq {

    private Integer chainId;

}
