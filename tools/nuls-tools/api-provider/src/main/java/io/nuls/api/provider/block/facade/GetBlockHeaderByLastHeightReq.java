package io.nuls.api.provider.block.facade;

import io.nuls.api.provider.BaseReq;

/**
 * @Author: zhoulijun
 * @Time: 2019-03-11 09:33
 * @Description:
 * 获取最新高度区块头
 * get block header by last height
 */
public class GetBlockHeaderByLastHeightReq extends BaseReq {

}
